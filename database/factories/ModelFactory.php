<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Category::class, function ($faker) {
    return [
        'name'        => $faker->sentence(3),
        'description' => $faker->boolean(25) ? $faker->sentence(6) : null,
        'icon'        => null,
    ];
});

$factory->define(App\Thread::class, function ($faker) {
    return [
        'category_id' => 'factory:App\Category',
        'is_locked'   => $faker->boolean(5),
        'is_sticky'   => $faker->boolean(5),
    ];
});

$factory->define(App\Post::class, function ($faker) {
    return [
        'thread_id' => 'factory:App\Thread',
        'title'     => $faker->sentence(3),
        'body'      => $faker->text(250),
    ];
});
