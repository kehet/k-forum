<?php

use App\Category;
use Illuminate\Database\Seeder;

class ThreadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::all();

        foreach ($categories as $category) {
            factory(App\Thread::class, mt_rand(1, 10))->create([
                'category_id' => $category->id,
            ]);
        }

    }
}
