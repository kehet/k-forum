<?php

use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create roles
        $adminGroup = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Admin',
            'slug' => 'admin',
        ]);

        $adminGroup
            ->addPermission('posts.create')
            ->addPermission('posts.own.edit')
            ->addPermission('posts.own.destroy')
            ->addPermission('posts.others.edit')
            ->addPermission('posts.others.destroy')
            ->addPermission('threads.create')
            ->addPermission('threads.own.destroy')
            ->addPermission('threads.others.destroy')
            ->save();

        $moderatorGroup = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Moderator',
            'slug' => 'moderator',
        ]);

        $moderatorGroup
            ->addPermission('posts.create')
            ->addPermission('posts.own.edit')
            ->addPermission('posts.own.destroy')
            ->addPermission('posts.others.edit')
            ->addPermission('posts.others.destroy')
            ->addPermission('threads.create')
            ->addPermission('threads.own.destroy')
            ->addPermission('threads.others.destroy')
            ->save();

        $userGroup = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'User',
            'slug' => 'user',
        ]);

        $userGroup
            ->addPermission('posts.create')
            ->addPermission('posts.own.edit')
            ->addPermission('posts.own.destroy')
            ->addPermission('threads.create')
            ->addPermission('threads.own.destroy')
            ->save();

        // create users
        $kehet = Sentinel::create([
            'email'      => 'kehet@kehet.com',
            'password'   => 'M4h3vAkAynHcHKG2MQ2ceEDjQ',
            'first_name' => 'Kehet',
        ]);

        $adminGroup->users()->attach($kehet);
        $this->activate($kehet);

        $faker = Faker\Factory::create('fi_FI');
        foreach (range(0, 5) as $i) {
            $user = Sentinel::create([
                'email'      => $faker->email,
                'password'   => str_random(10),
                'last_name'  => $faker->lastName,
                'first_name' => $faker->firstName,
            ]);

            if ($i < 2) {
                $moderatorGroup->users()->attach($user);
            } else {
                $userGroup->users()->attach($user);
            }
        }

    }

    private function activate($user)
    {
        $activation = Activation::create($user);
        Activation::complete($user, $activation->code);
    }
}
