<?php

use App\Thread;
use App\User;
use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $threads = Thread::all();
        $users = User::all();

        foreach ($threads as $thread) {
            factory(App\Post::class, mt_rand(1, 15))->create([
                'thread_id' => $thread->id,
                'user_id'   => $users->random()->id,
            ]);
        }
    }
}
