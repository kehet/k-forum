@extends('app')

@section('title')

@stop

@section('content')
    <div class="container">

        <ol class="breadcrumb">
            <li class="active">Forums</li>
        </ol>

        <table class="table forum">
            <thead>
            <tr>
                <th colspan="5">Title</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td class="icon text-center">
                        @if(!empty($category->icon))
                            {{ $category->icon }}
                        @else
                            <i class="fa fa-folder fa-2x text-muted"></i>
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('categories.threads.index', [$category->id]) }}">{{ $category->name }}</a>
                    </td>
                    <td class="text-center hidden-xs hidden-sm">
                        {{ $category->threads()->count() }}<br />
                        <small class="text-muted">Topics</small>
                    </td>
                    <td class="text-center hidden-xs hidden-sm">
                        {{ $category->posts()->count() }}<br />
                        <small class="text-muted">Posts</small>
                    </td>
                    <td class="hidden-xs hidden-sm text-right">
                        @if($category->posts()->count() == 0)
                            <span class="text-muted">No posts</span>
                        @else
                            <a href="{{ route('categories.threads.posts.index', [$category->id, $category->threads->first()->id]) }}">
                                {{ $category->threads->first()->posts->first()->title }}
                            </a><br />
                            <small class="text-muted">
                                by <a href="#">{{ $category->threads->first()->posts->first()->user->first_name }}</a>
                                <abbr title="{{ $category->threads->first()->posts->first()->created_at->format('d.m.Y H:i:s') }}">
                                    <time datetime="{{ $category->threads->first()->posts->first()->created_at->format('d.m.Y H:i:s') }}">
                                        {{ $category->threads->first()->posts->first()->created_at->diffForHumans() }}
                                    </time>
                                </abbr>
                            </small>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
@stop
