@extends('app')

@section('title')
    {{ $thread->posts->first()->title }} - {{ $thread->category->name }} -
@stop

@section('content')
    <div class="container">

        <ol class="breadcrumb">
            <li><a href="{{ route('categories.index') }}">Forums</a></li>
            <li><a href="{{ route('categories.threads.index', [$thread->category->id]) }}">{{ $thread->category->name }}</a></li>
            <li class="active">{{ $thread->posts->first()->title }}</li>
        </ol>

        <div class="page-header page-heading">
            <h1>{{ $thread->posts->first()->title }}</h1>
        </div>

        <p class="text-right">
            @if(Sentinel::check())
                @if(Sentinel::getUser()->hasAccess(['posts.create']))
                    <a class="btn btn-primary btn-lg" href="{{ route('categories.threads.posts.create', [$thread->category->id, $thread->id]) }}">New Reply</a>
                @else
                    You do not have permission to reply threads
                @endif
            @else
                <a href="{{ url('/auth/login') }}">Login</a> or <a href="{{ url('/auth/register') }}">Register</a> to reply thread
            @endif
        </p>

        <article class="forum-thread">
            @foreach($thread->posts as $i => $post)
                <section class="row row-eq-height">
                    <div class="col-md-3 user-info">
                        <img class="img-thumbnail" src="http://placehold.it/100" alt="{{ $post->user->first_name }}" /><br />
                        <a href="#">{{ $post->user->first_name }}</a><br />
                        {{ $post->user->roles->first()->name }}
                    </div>
                    <div class="col-md-9">
                        <h4 class="pull-left">
                            {{ $post->title }}<br />
                            <small class="text-muted">
                                <abbr title="{{ $post->created_at->format('d.m.Y H:i:s') }}">
                                    <time datetime="{{ $post->created_at->format('d.m.Y H:i:s') }}">{{ $post->created_at->diffForHumans() }}</time>
                                </abbr>
                            </small>
                        </h4>
                        <div class="pull-right" style="padding-top: 10px;">
                            @if(
                                Sentinel::check()
                                && (
                                    Sentinel::getUser()->id == $post->user->id
                                    && Sentinel::getUser()->hasAccess(['posts.own.edit'])
                                )
                                || (
                                    Sentinel::getUser()->id != $post->user->id
                                    && Sentinel::getUser()->hasAccess(['posts.others.edit'])
                                ))
                                <a href="{{ route('categories.threads.posts.edit', [$thread->category->id, $thread->id, $post->id]) }}" class="btn btn-primary btn-xs">Edit</a>
                            @endif

                            @if(
                                Sentinel::check()
                                && $i == 0
                                && (
                                    (
                                        Sentinel::getUser()->id == $post->user->id
                                        && Sentinel::getUser()->hasAccess(['threads.own.destroy'])
                                    )
                                    || (
                                        Sentinel::getUser()->id != $post->user->id
                                        && Sentinel::getUser()->hasAccess(['threads.others.destroy'])
                                    )
                                ))
                                {!! Form::open(['route' => ['categories.threads.destroy', $thread->category->id, $thread->id], 'method' => 'delete', 'style' => 'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                                {!! Form::close() !!}
                            @elseif(
                                Sentinel::check()
                                && $i != 0
                                && (
                                    (
                                        Sentinel::getUser()->id == $post->user->id
                                        && Sentinel::getUser()->hasAccess(['posts.own.destroy'])
                                    )
                                    || (
                                        Sentinel::getUser()->id != $post->user->id
                                        && Sentinel::getUser()->hasAccess(['posts.others.destroy'])
                                    )
                                ))
                                {!! Form::open(['route' => ['categories.threads.posts.destroy', $thread->category->id, $thread->id, $post->id], 'method' => 'delete', 'style' => 'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                                {!! Form::close() !!}
                            @endif
                        </div>
                        <div class="clearfix"></div>
                        {!! $post->body !!}
                    </div>
                </section>
            @endforeach
        </article>
    </div>
@stop
