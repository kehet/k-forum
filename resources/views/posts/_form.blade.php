<!-- Title form input -->
<div class="form-group">
    {!! Form::label('title', 'Title', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('title', null, ['placeholder' => 'Title', 'class' => 'form-control']) !!}
    </div>
</div>

<!-- Body form input -->
<div class="form-group">
    {!! Form::label('body', 'Body', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('body', null, ['placeholder' => 'Body', 'class' => 'form-control']) !!}
        <span class="help-block">
            <b>What is this?</b>
            Forum messages are parsed using <a href="http://spec.commonmark.org/0.19/">CommonMark</a>.
            Quick reference can be found <a href="http://en.wikipedia.org/wiki/Markdown#Example">on Wikipedia</a>.
        </span>
    </div>
</div>

<!-- Submit submit button -->
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        {!! Form::submit('Submit', ['class' => 'btn btn-primary form-control']) !!}
    </div>
</div>
