@extends('app')

@section('title')
    Reply - {{ $thread->posts->first()->title }} - {{ $thread->category->name }} -
@stop

@section('content')
    <div class="container">
        <ol class="breadcrumb where-am-i">
            <li><a href="{{ route('categories.index') }}">Forums</a></li>
            <li><a href="{{ route('categories.threads.index', [$thread->category->id]) }}">{{ $thread->category->name }}</a></li>
            <li><a href="{{ route('categories.threads.posts.index', [$thread->category->id, $thread->id]) }}">{{ $thread->posts->first()->title }}</a></li>
            <li class="active">New reply</li>
        </ol>

        <div class="page-header page-heading">
            <h1>
                New reply
            </h1>
        </div>

        @include('errors._form')
        {!! Form::model($post, ['route' => ['categories.threads.posts.store', $thread->category->id, $thread->id], 'class' => 'form-horizontal']) !!}
        @include('posts._form')
        {!! Form::close() !!}
    </div>
@stop
