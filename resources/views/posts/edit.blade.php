@extends('app')

@section('title')
    Edit {{ $post->title }} - {{ $post->thread->posts->first()->title }} - {{ $post->thread->category->name }} -
@stop

@section('content')
    <div class="container">
        <ol class="breadcrumb where-am-i">
            <li><a href="{{ route('categories.index') }}">Forums</a></li>
            <li><a href="{{ route('categories.threads.index', [$post->thread->category->id]) }}">{{ $post->thread->category->name }}</a></li>
            <li><a href="{{ route('categories.threads.posts.index', [$post->thread->category->id, $post->thread->id]) }}">{{ $post->thread->posts->first()->title }}</a></li>
            <li class="active">Edit post</li>
        </ol>

        <div class="page-header page-heading">
            <h1>
                Editing post <i>"{{ $post->title }}"</i>
            </h1>
        </div>

        @include('errors._form')

        {!! Form::model($post, ['route' => ['categories.threads.posts.update', $post->thread->category->id, $post->thread->id, $post->id], 'method' => 'patch', 'class' => 'form-horizontal']) !!}
        @include('posts._form')
        {!! Form::close() !!}
    </div>
@stop
