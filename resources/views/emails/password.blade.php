Hi there!

Someone (probably you) has requested password reset.

To reset your password, click this link:

{{ route('auth.password.reset', [$user->email, $reminder->code]) }}

- Admin
