Hi there!

Thank you for registering!

Please verify your e-mail address by clicking this link:

{{ route('auth.register.confirm', [$user->email, $activation->code]) }}

Thank you, and welcome to the community!

- Admin
