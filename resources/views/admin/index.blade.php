@extends('app')

@section('title')
    Admin panel -
@stop

@section('content')
    <div class="container">

        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Forums</a></li>
            <li class="active">Admin panel</li>
        </ol>

        <div class="page-header page-heading">
            <h1>Admin panel</h1>
        </div>

        <a href="{{ route('admin.categories.index') }}" class="btn btn-primary">Categories</a>
        <a href="{{ route('admin.users.index') }}" class="btn btn-primary">Users</a>
    </div>
@stop
