@extends('app')

@section('title')
    Edit user {{ $user->first_name }} {{ $user->last_name }} - Admin panel -
@stop

@section('content')
    <div class="container">

        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Forums</a></li>
            <li><a href="{{ route('admin.index') }}">Admin panel</a></li>
            <li><a href="{{ route('admin.users.index') }}">Users</a></li>
            <li class="active">Edit user</li>
        </ol>

        <div class="page-header page-heading">
            <h1>Edit user {{ $user->first_name }} {{ $user->last_name }}</h1>
        </div>

        @include('errors._form')

        {!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'patch', 'class' => 'form-horizontal']) !!}
        @include('admin.users._form')
        {!! Form::close() !!}
    </div>
@stop
