@extends('app')

@section('title')
    Users - Admin panel -
@stop

@section('content')
    <div class="container">

        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Forums</a></li>
            <li><a href="{{ route('admin.index') }}">Admin panel</a></li>
            <li class="active">List users</li>
        </ol>

        <div class="page-header page-heading">
            <h1>List users</h1>
        </div>

        <p class="text-right">
            <a class="btn btn-primary btn-lg" href="{{ route('admin.users.create') }}">New User</a>
        </p>

        <table class="table table-striped">
            <thead>
            <tr>
                <th class="text-center">Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Last login</th>
                <th>Created at</th>
                <th class="text-center">Is active</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td class="text-center">{{ $user->id }}</td>
                    <td><a href="{{ route('admin.users.show', [$user->id]) }}">{{ $user->first_name }} {{ $user->last_name }}</a></td>
                    <td>{{ $user->email }}</td>
                    <td>{!! isset($user->last_login) ? '<abbr title="'.$user->last_login->format('d.m.Y H:i:s').'">'.$user->last_login->diffForHumans().'</abbr>' : '<span class="text-muted">never</span>' !!}</td>
                    <td><abbr title="{{ $user->created_at->format('d.m.Y H:i:s') }}">{{ $user->created_at->diffForHumans() }}</abbr></td>
                    <td class="text-center">{!! Activation::completed($user) ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-times text-danger"></i>' !!}</td>
                    <td>
                        <a class="btn btn-xs btn-warning" href="{{ route('admin.users.edit', [$user->id]) }}">Edit</a>
                        {!! Form::open(['route' => ['admin.users.destroy', $user->id], 'method' => 'delete', 'style' => 'display:inline']) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop
