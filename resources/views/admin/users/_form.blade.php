<!-- First name form input -->
<div class="form-group">
    {!! Form::label('first_name', 'First name', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('first_name', null, ['placeholder' => 'First name', 'class' => 'form-control']) !!}
    </div>
</div>

<!-- Last name form input -->
<div class="form-group">
    {!! Form::label('last_name', 'Last name', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('last_name', null, ['placeholder' => 'Last name', 'class' => 'form-control']) !!}
    </div>
</div>

<!-- Email form input -->
<div class="form-group">
    {!! Form::label('email', 'Email', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control']) !!}
    </div>
</div>

<!-- Password form input -->
<div class="form-group">
    {!! Form::label('password', 'Password', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control']) !!}
        @if(isset($user->id))
            <p class="help-block">
                Leave empty if you don't want to change this.
            </p>
        @endif
    </div>
</div>

<!-- Role form input -->
<div class="form-group">
    {!! Form::label('role', 'Role', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::select('role[]', $roles, $currentRoles, ['multiple' => 'multiple', 'placeholder' => 'Role', 'class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="active" value="true" />
                User is activated
            </label>
        </div>
    </div>
</div>

<!-- Save submit button -->
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
    </div>
</div>
