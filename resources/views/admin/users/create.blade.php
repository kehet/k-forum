@extends('app')

@section('title')
    New user - Admin panel -
@stop

@section('content')
    <div class="container">

        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Forums</a></li>
            <li><a href="{{ route('admin.index') }}">Admin panel</a></li>
            <li><a href="{{ route('admin.users.index') }}">Users</a></li>
            <li class="active">New user</li>
        </ol>

        <div class="page-header page-heading">
            <h1>New user</h1>
        </div>

        @include('errors._form')

        {!! Form::model($user = new App\User, ['route' => ['admin.users.store'], 'class' => 'form-horizontal']) !!}
        @include('admin.users._form')
        {!! Form::close() !!}
    </div>
@stop
