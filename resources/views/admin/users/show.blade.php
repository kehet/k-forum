@extends('app')

@section('title')
    User {{ $user->first_name }} {{ $user->last_name }} - Admin panel -
@stop

@section('content')
    <div class="container">

        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Forums</a></li>
            <li><a href="{{ route('admin.index') }}">Admin panel</a></li>
            <li><a href="{{ route('admin.users.index') }}">Users</a></li>
            <li class="active">Show user</li>
        </ol>

        <div class="page-header page-heading">
            <h1>User {{ $user->first_name }} {{ $user->last_name }}</h1>
        </div>

        <p class="text-right">
            <a class="btn btn-primary btn-lg" href="{{ route('admin.users.edit', [$user->id]) }}">Edit User</a>
        </p>

        <dl class="dl-horizontal">
            <dt>Roles</dt>
            <dd>
                @foreach($user->roles as $i => $role)
                    {{ $role->name }}{{ $i < count($user->roles)-1 ? ', ' : '' }}
                @endforeach
            </dd>

            <dt>Email</dt>
            <dd>{{ $user->email }}</dd>

            <dt>Last login</dt>
            <dd>{!! isset($user->last_login) ? '<abbr title="'.$user->last_login->format('d.m.Y H:i:s').'">'.$user->last_login->diffForHumans().'</abbr>' : '<span class="text-muted">never</span>' !!}</dd>

            <dt>Created at</dt>
            <dd><abbr title="{{ $user->created_at->format('d.m.Y H:i:s') }}">{{ $user->created_at->diffForHumans() }}</abbr></dd>

            <dt>Last update at</dt>
            <dd><abbr title="{{ $user->updated_at->format('d.m.Y H:i:s') }}">{{ $user->updated_at->diffForHumans() }}</abbr></dd>

            <dt>Active</dt>
            <dd>{!! $activation !== false && $activation->completed != false ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-times text-danger"></i>' !!}</dd>
        </dl>

        <hr />

        <h2>Lates forum posts</h2>

        <article class="forum-thread">
            @foreach($posts as $i => $post)
                <section class="row row-eq-height">
                    <div class="col-md-3 user-info">
                        <img class="img-thumbnail" src="http://placehold.it/100" alt="{{ $user->first_name }}" /><br />
                        <a href="#">{{ $user->first_name }}</a><br />
                        {{ $user->roles->first()->name }}
                    </div>
                    <div class="col-md-9">
                        <h4 class="pull-left">
                            <a href="{{ route('categories.threads.posts.index', [$post->thread->category->id, $post->thread->id]) }}">{{ $post->title }}</a><br />
                            <small class="text-muted">
                                <abbr title="{{ $post->created_at->format('d.m.Y H:i:s') }}">
                                    <time datetime="{{ $post->created_at->format('d.m.Y H:i:s') }}">{{ $post->created_at->diffForHumans() }}</time>
                                </abbr>
                            </small>
                        </h4>
                        <div class="pull-right" style="padding-top: 10px;">
                            <a href="{{ route('categories.threads.posts.edit', [$post->thread->category->id, $post->thread->id, $post->id]) }}" class="btn btn-primary btn-xs">Edit</a>

                            @if($i == 0)
                                {!! Form::open(['route' => ['categories.threads.destroy', $post->thread->category->id, $post->thread->id], 'method' => 'delete', 'style' => 'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                                {!! Form::close() !!}
                            @elseif($i != 0)
                                {!! Form::open(['route' => ['categories.threads.posts.destroy', $post->thread->category->id, $post->thread->id, $post->id], 'method' => 'delete', 'style' => 'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                                {!! Form::close() !!}
                            @endif
                        </div>
                        <div class="clearfix"></div>
                        {!! $post->body !!}
                    </div>
                </section>
            @endforeach
        </article>
    </div>
@stop
