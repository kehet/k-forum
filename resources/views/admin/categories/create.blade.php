@extends('app')

@section('title')
    New category - Admin panel -
@stop

@section('content')
    <div class="container">

        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Forums</a></li>
            <li><a href="{{ route('admin.index') }}">Admin panel</a></li>
            <li><a href="{{ route('admin.categories.index') }}">Categories</a></li>
            <li class="active">New category</li>
        </ol>

        <div class="page-header page-heading">
            <h1>New category</h1>
        </div>

        @include('errors._form')

        {!! Form::model($category = new \App\Post, ['route' => ['admin.categories.store'], 'class' => 'form-horizontal']) !!}
        @include('admin.categories._form')
        {!! Form::close() !!}
    </div>
@stop
