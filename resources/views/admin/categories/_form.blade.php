<!-- Name form input -->
<div class="form-group">
    {!! Form::label('name', 'Name', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<!-- Description form input -->
<div class="form-group">
    {!! Form::label('description', 'Description', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('description', null, ['placeholder' => 'Description', 'class' => 'form-control']) !!}
    </div>
</div>

<!-- Icon form input -->
<div class="form-group">
    {!! Form::label('icon', 'Icon', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('icon', null, ['placeholder' => 'Icon', 'class' => 'form-control']) !!}
    </div>
</div>

<!-- Save submit button -->
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
    </div>
</div>
