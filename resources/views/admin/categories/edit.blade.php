@extends('app')

@section('title')
    Edit category {{ $category->name }} - Admin panel -
@stop

@section('content')
    <div class="container">

        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Forums</a></li>
            <li><a href="{{ route('admin.index') }}">Admin panel</a></li>
            <li><a href="{{ route('admin.categories.index') }}">Categories</a></li>
            <li class="active">Edit category</li>
        </ol>

        <div class="page-header page-heading">
            <h1>Edit category {{ $category->name }}</h1>
        </div>

        @include('errors._form')

        {!! Form::model($category, ['route' => ['admin.categories.update', $category->id], 'method' => 'patch', 'class' => 'form-horizontal']) !!}
        @include('admin.categories._form')
        {!! Form::close() !!}
    </div>
@stop
