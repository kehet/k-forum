@extends('app')

@section('title')
    Categories - Admin panel -
@stop

@section('content')
    <div class="container">

        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Forums</a></li>
            <li><a href="{{ route('admin.index') }}">Admin panel</a></li>
            <li class="active">List categories</li>
        </ol>

        <div class="page-header page-heading">
            <h1>List categories</h1>
        </div>

        <p class="text-right">
            <a class="btn btn-primary btn-lg" href="{{ route('admin.categories.create') }}">New Category</a>
        </p>

        <table class="table table-striped">
            <thead>
            <tr>
                <th class="text-center">Id</th>
                <th>Name</th>
                <th>Description</th>
                <th class="text-center">Icon</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td class="text-center">{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td>{!! isset($category->description) ? $category->description : '<i>Empty</i>' !!}</td>
                    <td class="text-center">
                        @if(!empty($category->icon))
                            {{ $category->icon }}
                        @else
                            <i class="fa fa-folder fa-2x text-muted"></i>
                        @endif
                    </td>
                    <td>
                        <a class="btn btn-xs btn-warning" href="{{ route('admin.categories.edit', [$category->id]) }}">Edit</a>
                        {!! Form::open(['route' => ['admin.categories.destroy', $category->id], 'method' => 'delete', 'style' => 'display:inline']) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop
