@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                        @include('errors._form')

                        {!! Form::open(['route' => 'auth.register', 'class' => 'form-horizontal']) !!}
                        <!-- First Name form input -->
                            <div class="form-group">
                                {!! Form::label('first_name', 'First Name', ['class' => 'control-label col-sm-2']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text('first_name', null, ['placeholder' => 'First Name', 'class' => 'form-control', 'required' => 'required']) !!}
                                </div>
                            </div>

                        <!-- Last Name form input -->
                            <div class="form-group">
                                {!! Form::label('last_name', 'Last Name', ['class' => 'control-label col-sm-2']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text('last_name', null, ['placeholder' => 'Last Name', 'class' => 'form-control']) !!}
                                </div>
                            </div>

                        <!-- Email form input -->
                            <div class="form-group">
                                {!! Form::label('email', 'Email', ['class' => 'control-label col-sm-2']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control', 'required' => 'required']) !!}
                                </div>
                            </div>

                        <!-- Password form input -->
                            <div class="form-group">
                                {!! Form::label('password', 'Password', ['class' => 'control-label col-sm-2']) !!}
                                <div class="col-sm-10">
                                    {!! Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control', 'required' => 'required']) !!}
                                </div>
                            </div>

                        <!-- Register submit button -->
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    {!! Form::submit('Register', ['class' => 'btn btn-primary form-control']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
