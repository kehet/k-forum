@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Reset Password</div>
                    <div class="panel-body">
                        @include('errors._form')

                        {!! Form::open(['route' => 'auth.password.reset.post', 'class' => 'form-horizontal']) !!}

                        <!-- Email form input -->
                            <div class="form-group">
                                {!! Form::label('email', 'Email', ['class' => 'control-label col-sm-2']) !!}
                                <div class="col-sm-10">
                                    @if(empty($email))
                                        {!! Form::text('email', $email, ['placeholder' => 'Password', 'class' => 'form-control', 'required' => 'required']) !!}
                                    @else
                                        <p class="form-control-static">
                                            {{ $email }}
                                        </p>
                                        {!! Form::hidden('email', $email) !!}
                                    @endif
                                </div>
                            </div>

                        <!-- Token form input -->
                        <div class="form-group">
                            {!! Form::label('token', 'Token', ['class' => 'control-label col-sm-2']) !!}
                            <div class="col-sm-10">
                                @if(empty($token))
                                    {!! Form::text('token', $token, ['placeholder' => 'Token', 'class' => 'form-control', 'required' => 'required']) !!}
                                @else
                                    <p class="form-control-static">
                                        {{ $token }}
                                    </p>
                                    {!! Form::hidden('token', $token) !!}
                                @endif
                            </div>
                        </div>

                        <!-- Password form input -->
                        <div class="form-group">
                            {!! Form::label('password', 'New password', ['class' => 'control-label col-sm-2']) !!}
                            <div class="col-sm-10">
                                {!! Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control', 'required' => 'required']) !!}
                            </div>
                        </div>

                        <!-- Reset password submit button -->
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    {!! Form::submit('Reset password', ['class' => 'btn btn-primary form-control']) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
