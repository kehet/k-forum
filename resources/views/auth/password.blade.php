@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Reset Password</div>
                    <div class="panel-body">
                        @include('errors._form')

                        {!! Form::open(['route' => 'auth.password.post', 'class' => 'form-horizontal']) !!}

                        <!-- Email form input -->
                            <div class="form-group">
                                {!! Form::label('email', 'Email', ['class' => 'control-label col-sm-2']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control']) !!}
                                </div>
                            </div>

                        <!-- Reset password submit button -->
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    {!! Form::submit('Reset password', ['class' => 'btn btn-primary form-control']) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
