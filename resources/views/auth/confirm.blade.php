@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Confirm email</div>
                    <div class="panel-body">
                        @include('errors._form')

                        {!! Form::open(['route' => 'auth.register.confirm.post', 'class' => 'form-horizontal']) !!}

                            <!-- Email form input -->
                            <div class="form-group">
                                {!! Form::label('email', 'Email', ['class' => 'control-label col-sm-2']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text('email', $email, ['placeholder' => 'Email', 'class' => 'form-control']) !!}
                                </div>
                            </div>

                            <!-- Token form input -->
                            <div class="form-group">
                                {!! Form::label('token', 'Token', ['class' => 'control-label col-sm-2']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text('token', $token, ['placeholder' => 'Token', 'class' => 'form-control']) !!}
                                </div>
                            </div>

                            <!-- Confirm submit button -->
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    {!! Form::submit('Confirm', ['class' => 'btn btn-primary form-control']) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
