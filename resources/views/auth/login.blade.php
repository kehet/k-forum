@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">
                        @include('errors._form')

                        {!! Form::open(['route' => 'auth.login', 'class' => 'form-horizontal']) !!}

                        <!-- Email form input -->
                            <div class="form-group">
                                {!! Form::label('email', 'Email', ['class' => 'control-label col-sm-2']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control']) !!}
                                    <p class="help-block">
                                        <a class="btn btn-link" href="{{ route('auth.password') }}">Forgot Your Password?</a>
                                    </p>
                                </div>
                            </div>

                        <!-- Password form input -->
                            <div class="form-group">
                                {!! Form::label('password', 'Password', ['class' => 'control-label col-sm-2']) !!}
                                <div class="col-sm-10">
                                    {!! Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control']) !!}
                                </div>
                            </div>

                        <!-- Remember me form input -->
                            <div class="form-group">
                                {!! Form::label('remember', 'Remember me', ['class' => 'control-label col-sm-2']) !!}
                                <div class="col-sm-10">
                                    {!! Form::checkbox('remember', null, ['class' => 'form-control']) !!} Remember me
                                </div>
                            </div>

                        <!-- Login submit button -->
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    {!! Form::submit('Login', ['class' => 'btn btn-primary form-control']) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
