@extends('app')

@section('title')
    New thread - {{ $category->name }} -
@stop

@section('content')
    <div class="container">
        <ol class="breadcrumb where-am-i">
            <li><a href="{{ route('categories.index') }}">Forums</a></li>
            <li><a href="{{ route('categories.threads.index', [$category->id]) }}">{{ $category->name }}</a></li>
            <li class="active">New thread</li>
        </ol>

        <div class="page-header page-heading">
            <h1>
                New thread
            </h1>
        </div>

        @include('errors._form')

        {!! Form::model($post = new \App\Post, ['route' => ['categories.threads.store', $category->id], 'class' => 'form-horizontal']) !!}
        @include('posts._form')
        {!! Form::close() !!}
    </div>
@stop
