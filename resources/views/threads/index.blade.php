@extends('app')

@section('title')
    {{ $category->name }} -
@stop

@section('content')
    <div class="container">

        <ol class="breadcrumb">
            <li><a href="{{ route('categories.index') }}">Forums</a></li>
            <li class="active">{{ $category->name }}</li>
        </ol>

        <div class="page-header page-heading">
            <h1>{{ $category->name }}</h1>
        </div>

        <p class="text-right">
            @if(Sentinel::check())
                @if(Sentinel::getUser()->hasAccess(['threads.create']))
                    <a class="btn btn-primary btn-lg" href="{{ route('categories.threads.create', [$category->id]) }}">New Thread</a>
                @else
                    You do not have permission to create new threads
                @endif
            @else
                <a href="{{ url('/auth/login') }}">Login</a> or <a href="{{ url('/auth/register') }}">Register</a> to create thread
            @endif
        </p>

        <table class="table forum">
            <thead>
            <tr>
                <th colspan="4">{{ $category->name }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($category->threads as $thread)
                <tr>
                    <td class="icon text-center">
                        <i class="fa fa-heart fa-2x text-danger"></i>
                    </td>
                    <td>
                        <a href="{{ route('categories.threads.posts.index', [$category->id, $thread->id]) }}">{{ $thread->posts->first()->title }}</a><br />
                        <small class="text-muted">
                            by <a href="#">{{ $thread->posts->first()->user->first_name }}</a>
                            <abbr title="{{ $thread->posts->first()->created_at->format('d.m.Y H:i:s') }}">
                                <time datetime="{{ $thread->posts->first()->created_at->format('d.m.Y H:i:s') }}">
                                    {{ $thread->posts->first()->created_at->diffForHumans() }}
                                </time>
                            </abbr>
                        </small>
                    </td>
                    <td class="text-center hidden-xs hidden-sm">
                        {{ $thread->posts->count() - 1 }}<br />
                        <small class="text-muted">Replies</small>
                    </td>
                    <td class="hidden-xs hidden-sm text-right">
                        @if($thread->posts->count() <= 1)
                            <span class="text-muted">
                                No replies
                            </span>
                        @else
                            Last post by <a href="#">{{ $thread->posts->last()->user->first_name }}</a><br />
                            <small class="text-muted">
                                <abbr title="{{ $thread->posts->last()->created_at->format('d.m.Y H:i:s') }}">
                                    <time datetime="{{ $thread->posts->last()->created_at->format('d.m.Y H:i:s') }}">
                                        {{ $thread->posts->last()->created_at->diffForHumans() }}
                                    </time>
                                </abbr>
                            </small>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop
