<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\PostRequest;
use App\Post;
use App\Thread;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class PostsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['expect' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param $categoryId
     * @param $threadId
     * @return Response
     */
    public function index($categoryId, $threadId)
    {
        $thread = Thread::with([
            'category' => function ($query) use ($categoryId) {
                $query->where('id', $categoryId);
            },
            'posts'    => function ($query) {
                $query->orderBy('created_at', 'asc');
            }
        ])
            ->findOrFail($threadId);

        return view('posts.index', compact('thread'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $categoryId
     * @param $threadId
     * @return Response
     */
    public function create($categoryId, $threadId)
    {
        if (!Sentinel::getUser()->hasAccess(['posts.create'])) {
            abort(401);
        }

        $thread = Thread::with([
            'category' => function ($query) use ($categoryId) {
                $query->where('id', $categoryId);
            }
        ])
            ->findOrFail($threadId);

        $post = new Post();
        $post->title = 'Re: ' . $thread->posts->first()->title;

        return view('posts.create', compact('thread', 'post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $categoryId
     * @param $threadId
     * @param PostRequest $request
     * @return Response
     */
    public function store($categoryId, $threadId, PostRequest $request)
    {
        if (!Sentinel::getUser()->hasAccess(['posts.create'])) {
            abort(401);
        }

        $thread = Thread::with([
            'category' => function ($query) use ($categoryId) {
                $query->where('id', $categoryId);
            }
        ])
            ->findOrFail($threadId);

        $post = new Post($request->all());
        $post->user_id = Sentinel::getUser()->id;
        $post->thread_id = $thread->id;
        $post->save();

        return redirect()->route('categories.threads.posts.index', [$thread->category->id, $thread->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param $categoryId
     * @param $threadId
     * @param $postId
     * @return Response
     */
    public function show($categoryId, $threadId, $postId)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $categoryId
     * @param $threadId
     * @param $postId
     * @return Response
     */
    public function edit($categoryId, $threadId, $postId)
    {
        $post = Post::with([
            'thread'          => function ($query) use ($threadId) {
                $query->where('id', $threadId);
            },
            'thread.category' => function ($query) use ($categoryId) {
                $query->where('id', $categoryId);
            },
        ])
            ->findOrFail($postId);

        if (
            (Sentinel::getUser()->id == $post->user->id && !Sentinel::getUser()->hasAccess(['posts.own.edit']))
            || (Sentinel::getUser()->id != $post->user->id && !Sentinel::getUser()->hasAccess(['posts.others.edit']))
        ) {
            abort(401);
        }

        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $categoryId
     * @param $threadId
     * @param $postId
     * @param PostRequest $request
     * @return Response
     */
    public function update($categoryId, $threadId, $postId, PostRequest $request)
    {
        $post = Post::with([
            'thread'          => function ($query) use ($threadId) {
                $query->where('id', $threadId);
            },
            'thread.category' => function ($query) use ($categoryId) {
                $query->where('id', $categoryId);
            },
        ])
            ->findOrFail($postId);

        if (
            (Sentinel::getUser()->id == $post->user->id && !Sentinel::getUser()->hasAccess(['posts.own.edit']))
            || (Sentinel::getUser()->id != $post->user->id && !Sentinel::getUser()->hasAccess(['posts.others.edit']))
        ) {
            abort(401);
        }

        $post->update($request->all());

        return redirect()->route('categories.threads.posts.index', [$post->thread->category->id, $post->thread->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $categoryId
     * @param $threadId
     * @param $postId
     * @return Response
     */
    public function destroy($categoryId, $threadId, $postId)
    {
        $post = Post::with([
            'thread'          => function ($query) use ($threadId) {
                $query->where('id', $threadId);
            },
            'thread.category' => function ($query) use ($categoryId) {
                $query->where('id', $categoryId);
            },
        ])
            ->findOrFail($postId);

        // don't allow deleting threads here
        if ($post->id == $post->thread->posts->first()->id) {
            abort(401);
        }

        if (
            // own post and no rights to destroy it
            (Sentinel::getUser()->id == $post->user->id && !Sentinel::getUser()->hasAccess(['posts.own.destroy']))

            // others post and no rights to destroy it
            || (Sentinel::getUser()->id != $post->user->id && !Sentinel::getUser()->hasAccess(['posts.others.destroy']))
        ) {
            abort(401);
        }

        $post->delete();

        return redirect()->route('categories.threads.posts.index', [$post->thread->category->id, $post->thread->id]);
    }
}
