<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests;

class CategoriesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['expect' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::with([
            'threads'       => function ($query) {
                $query->orderBy('updated_at', 'desc');
            },
            'threads.posts' => function ($query) {
                $query->orderBy('created_at', 'asc');
            },
            'threads.posts.user',
        ])->get();

        return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $categoryId
     * @return Response
     */
    public function show($categoryId)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $categoryId
     * @return Response
     */
    public function edit($categoryId)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $categoryId
     * @return Response
     */
    public function update($categoryId)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $categoryId
     * @return Response
     */
    public function destroy($categoryId)
    {
        //
    }
}
