<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests;
use App\Http\Requests\PostRequest;
use App\Post;
use App\Thread;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class ThreadsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['expect' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param $categoryId
     * @return Response
     */
    public function index($categoryId)
    {
        $category = Category::with([
            'threads' => function ($query) {
                $query->orderBy('updated_at', 'desc');
            },
            'threads.posts' => function ($query) {
                $query->orderBy('created_at', 'asc');
            },
            'threads.posts.user',
        ])
            ->findOrFail($categoryId);

        return view('threads.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $categoryId
     * @return Response
     */
    public function create($categoryId)
    {
        if (!Sentinel::getUser()->hasAccess(['threads.create'])) {
            abort(401);
        }

        $category = Category::findOrFail($categoryId);

        return view('threads.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $categoryId
     * @param PostRequest $request
     * @return Response
     */
    public function store($categoryId, PostRequest $request)
    {
        if (!Sentinel::getUser()->hasAccess(['threads.create'])) {
            abort(401);
        }

        $category = Category::findOrFail($categoryId);

        $thread = new Thread();
        $thread->category_id = $category->id;
        $thread->save();

        $post = new Post($request->all());
        $post->user_id = Sentinel::getUser()->id;
        $post->thread_id = $thread->id;
        $post->save();

        return redirect()->route('categories.threads.posts.index', [$category->id, $thread->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param $categoryId
     * @param $threadId
     * @return Response
     */
    public function show($categoryId, $threadId)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $categoryId
     * @param $threadId
     * @return Response
     */
    public function edit($categoryId, $threadId)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $categoryId
     * @param $threadId
     * @return Response
     */
    public function update($categoryId, $threadId)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $categoryId
     * @param $threadId
     * @return Response
     */
    public function destroy($categoryId, $threadId)
    {
        $thread = Thread::with([
            'category' => function ($query) use ($categoryId) {
                $query->where('id', $categoryId);
            },
            'posts'    => function ($query) {
                $query->orderBy('created_at', 'asc');
            }
        ])
            ->findOrFail($threadId);

        if (
            // own thread and no rights to destroy it
            (Sentinel::getUser()->id == $thread->posts->first()->user->id && !Sentinel::getUser()->hasAccess(['threads.own.destroy']))

            // others thread and no rights to destroy it
            || (Sentinel::getUser()->id != $thread->posts->first()->user->id && !Sentinel::getUser()->hasAccess(['threads.others.destroy']))
        ) {
            abort(401);
        }

        $thread->posts()->delete();
        $thread->delete();

        return redirect()->route('categories.threads.index', [$thread->category->id]);
    }
}
