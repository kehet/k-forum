<?php

namespace App\Http\Controllers;

use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AuthenticationController extends Controller
{

    public function getLogin()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required',
        ]);

        try {
            $user = Sentinel::authenticate($request->only('email', 'password'));

            if ($user !== false) {
                return redirect()->intended('/');
            }
        } catch (ThrottlingException $e) {
            return $this->failLogin($request, $e->getMessage());
        } catch (NotActivatedException $e) {
            return $this->failLogin($request, $e->getMessage());
        }

        return $this->failLogin($request, 'These credentials do not match our records');
    }

    private function failLogin($request, $message)
    {
        return back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => $message,
            ]);
    }

    public function getLogout()
    {
        Sentinel::logout();

        return redirect()->route('home');
    }

    public function getRegister()
    {
        return view('auth.register');
    }

    public function postRegister(Request $request)
    {
        $this->validate($request, [
            'email'      => 'required|email|max:255|unique:users',
            'password'   => 'required|min:6',
            'first_name' => 'required|max:255',
            'last_name'  => 'max:255',
        ]);

        $user = Sentinel::register($request->all());
        $activation = Activation::create($user);

        Mail::send(['text' => 'emails.confirm'], compact('user', 'activation'), function ($message) use ($user) {
            $message
                ->from('noreply@kehet.com', 'Kehet')
                ->to($user->email, $user->first_name . ' ' . $user->last_name)
                ->subject('Please confirm your account');
        });

        return redirect()->route('auth.register.confirm', ['email' => $user->email]);
    }

    public function getConfirm($email = null, $token = null)
    {
        if (empty($email) || empty($token)) {
            return view('auth.confirm', compact('email', 'token'));
        }

        return $this->tryActivate($email, $token);
    }

    private function tryActivate($email, $token)
    {
        $user = Sentinel::findByCredentials([
            'email' => $email,
        ]);

        if (Activation::complete($user, $token)) {
            return redirect()->route('auth.login');
        } else {
            return redirect()
                ->route('auth.login')
                ->withInput()
                ->withErrors([
                    'token' => 'Token was incorrect',
                ]);
        }
    }

    public function postConfirm(Request $request)
    {
        return $this->tryActivate($request->input('email'), $request->input('token'));
    }

    public function getPassword()
    {
        return view('auth.password');
    }

    public function postPassword(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $user = Sentinel::findByCredentials([
            'email' => $request->input('email'),
        ]);

        $reminder = Reminder::create($user);

        Mail::send(['text' => 'emails.password'], compact('user', 'reminder'), function ($message) use ($user) {
            $message
                ->from('noreply@kehet.com', 'Kehet')
                ->to($user->email, $user->first_name . ' ' . $user->last_name)
                ->subject('Your password was reseted');
        });

        return redirect()->route('auth.password.reset', ['email' => $user->email]);
    }

    public function getReset($email = null, $token = null)
    {
        return view('auth.reset', compact('email', 'token'));
    }

    public function postReset(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|email|max:255|exists:users',
            'password' => 'required|min:6',
        ]);

        $user = Sentinel::findByCredentials([
            'email' => $request->input('email'),
        ]);

        $reminder = Reminder::complete($user, $request->input('token'), $request->input('password'));

        if ($reminder) {
            return redirect()->route('auth.login');
        } else {
            return redirect()
                ->route('auth.password.reset', [$request->input('email'), $request->input('token')])
                ->withInput()
                ->withErrors([
                    'token' => 'Token was incorrect',
                ]);
        }
    }

}
