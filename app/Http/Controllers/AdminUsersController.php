<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Post;
use App\User;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class AdminUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $_roles = Sentinel::getRoleRepository()->all();

        $roles = [];
        foreach ($_roles as $role) {
            $roles[ $role->id ] = $role->name;
        }

        $currentRoles = null;

        return view('admin.users.create', compact('roles', 'currentRoles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $request
     * @return Response
     */
    public function store(UserRequest $request)
    {
        $user = Sentinel::register($request->only([
            'first_name',
            'last_name',
            'email',
            'password',
        ]), $request->has('active'));

        if (count($request->get('role')) > 0) {
            foreach ($request->get('role') as $role) {
                Sentinel::findRoleById($role)->users()->attach($user);
            }
        }

        return redirect()->route('admin.users.show', [$user->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $user = Sentinel::findById($id);
        $activation = Activation::exists($user);

        $posts = Post::with([
            'thread',
            'thread.category',
        ])
            ->where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('admin.users.show', compact('user', 'activation', 'posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = Sentinel::findById($id);

        $_roles = Sentinel::getRoleRepository()->all();

        $roles = [];
        foreach ($_roles as $role) {
            $roles[ $role->id ] = $role->name;
        }

        $currentRoles = [];
        foreach ($user->roles as $role) {
            $currentRoles[] = $role->id;
        }

        return view('admin.users.edit', compact('user', 'roles', 'currentRoles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param UserRequest $request
     * @return Response
     */
    public function update($id, UserRequest $request)
    {
        $user = Sentinel::findById($id);

        $credentials = $request->only([
            'first_name',
            'last_name',
            'email',
        ]);

        if ($request->has('password')) {
            $credentials['password'] = $request->get('password');
        }

        $valid = Sentinel::getUserRepository()->validForUpdate($user, $credentials);

        if ($valid === false) {
            echo 'error';

            return false;
        }

        Sentinel::getUserRepository()->update($user, $credentials);

        if ($request->get('active') != Activation::completed($user)) {
            if ($request->get('active')) {
                $activation = Activation::create($user);
                Activation::complete($user, $activation->code);
            } else {
                Activation::remove($user);
            }
        }

        $roles = Sentinel::getRoleRepository()->all();
        $wantedRoles = $request->get('role');

        foreach ($roles as $role) {
            if (in_array($role->id, $wantedRoles) && !$user->inRole($role)) {
                $role->users()->attach($user);
            } elseif (!in_array($role->id, $wantedRoles) && $user->inRole($role)) {
                $role->users()->detach($user);
            }
        }

        return redirect()->route('admin.users.show', [$user->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = Sentinel::findById($id);
        $user->delete();

        return redirect()->route('admin.users.index');
    }
}
