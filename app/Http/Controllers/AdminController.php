<?php

namespace App\Http\Controllers;

use App\Http\Requests;

class AdminController extends Controller
{
    /**
     * @return Response
     */
    public function index()
    {
        return view('admin.index');
    }
}
