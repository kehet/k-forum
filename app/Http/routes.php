<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'as'   => 'home',
    'uses' => function () {
        return redirect()->route('categories.index');
    }
]);

Route::resource('categories', 'CategoriesController');
Route::resource('categories.threads', 'ThreadsController');
Route::resource('categories.threads.posts', 'PostsController');

Route::group([
    'prefix'     => 'admin',
    'middleware' => 'admin',
], function () {
    Route::get('index', [
        'as'   => 'admin.index',
        'uses' => 'AdminController@index',
    ]);

    Route::resource('categories', 'AdminCategoriesController', [
        'expect' => ['show',],
    ]);

    Route::resource('users', 'AdminUsersController');
});

// Authentication routes
Route::get('auth/login', [
    'as'   => 'auth.login',
    'uses' => 'AuthenticationController@getLogin',
]);
Route::post('auth/login', [
    'as'   => 'auth.login.post',
    'uses' => 'AuthenticationController@postLogin',
]);
Route::get('auth/logout', [
    'as'   => 'auth.logout',
    'uses' => 'AuthenticationController@getLogout',
]);

// Registration routes
Route::get('auth/register', [
    'as'   => 'auth.register',
    'uses' => 'AuthenticationController@getRegister',
]);
Route::post('auth/register', [
    'as'   => 'auth.register.post',
    'uses' => 'AuthenticationController@postRegister',
]);
Route::get('auth/register/confirm/{email?}/{token?}', [
    'as'   => 'auth.register.confirm',
    'uses' => 'AuthenticationController@getConfirm',
]);
Route::post('auth/register/confirm', [
    'as'   => 'auth.register.confirm.post',
    'uses' => 'AuthenticationController@postConfirm',
]);

// Password reset routes
Route::get('auth/password/email', [
    'as'   => 'auth.password',
    'uses' => 'AuthenticationController@getPassword',
]);
Route::post('auth/password/email', [
    'as'   => 'auth.password.post',
    'uses' => 'AuthenticationController@postPassword',
]);
Route::get('auth/password/reset/{email?}/{token?}', [
    'as'   => 'auth.password.reset',
    'uses' => 'AuthenticationController@getReset',
]);
Route::post('auth/password/reset', [
    'as'   => 'auth.password.reset.post',
    'uses' => 'AuthenticationController@postReset',
]);
