<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

    use SoftDeletes;

    protected $table = 'categories';

    protected $fillable = [
        'name',
        'description',
        'icon',
    ];

    protected $dates = [
        'deleted_at',
    ];

    public function threads()
    {
        return $this->hasMany('App\Thread');
    }

    public function posts()
    {
        return $this->hasManyThrough('App\Post', 'App\Thread');
    }

}
