## List of permissions implemented

| Permission             | Admin | Mod   | User  |
|:-----------------------|:-----:|:-----:|:-----:|
| post.create            | true  | true  | true  |
| post.own.edit          | true  | true  | true  |
| post.own.destroy       | true  | true  | true  |
| post.others.edit       | true  | true  |       |
| post.others.destroy    | true  | true  |       |
| threads.create         | true  | true  | true  |
| threads.own.destroy    | true  | true  | true  |
| threads.others.destroy | true  | true  |       |

