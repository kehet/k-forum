# Kehet/k-forum
This is simple forum build with Laravel and few more awesome library.

## Version history

### v1.2.0
Admin panel, now possible to edit users and categories using nice gui

### v1.1.1
Completed moving to Sentinel: password reset and registration

### v1.1.0
* (Admin) Multiple user levels
* (Admin) Able to edit / remove posts

### v1.0.0
This is first public remade release

## Roadmap
* Able to login using social media
* Able to decorate messages
* (Admin) Able to edit categories using UI
* ~~(Admin) Multiple user levels~~
* ~~(Admin) Able to edit / remove posts~~
* ~~Able to edit own posts~~
* ~~Categories~~

## Security
It's probably best to not to use this in production environment. No security patches guaranteed.

## License
**Kehet/k-forum** is licenced under GNU GENERAL PUBLIC LICENSE v3 or later. See ``LICENSE`` file for more details.

## Screenshots
![](forum.png)

![](thread.png)

![](admin-users.png)
